DROP TABLE certificate IF EXISTS;
DROP TABLE device IF EXISTS;

CREATE TABLE device (
  id INTEGER,
  PRIMARY KEY (id)
);

CREATE TABLE certificate (
  id INTEGER,
  start_date DATE,
  end_date DATE,
  device_id INTEGER,

  PRIMARY KEY (id),
  FOREIGN KEY (device_id) REFERENCES device(id)
);

INSERT INTO device(id)
VALUES (1), (2), (3);

INSERT INTO certificate(id, start_date, end_date, device_id)
VALUES
  (1, '2019-01-01', '2019-12-31', 1),
  (2, '2020-01-01', '2020-12-31', 1),
  (3, '2021-01-01', '2021-12-31', 1), -- dev. 1 is active
  (5, '2017-01-01', '2025-12-31', 2), -- active, dev. 2
  (4, '2015-01-01', '2016-12-31', 3); -- expired, dev. 3