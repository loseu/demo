package com.example.infrastructure.repository;

import com.example.infrastructure.entity.Certificate;
import org.springframework.stereotype.Repository;

@Repository
public interface CertificateRepository extends EntityRepository<Certificate, Integer> {

}
