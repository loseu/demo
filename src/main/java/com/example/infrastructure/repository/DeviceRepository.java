package com.example.infrastructure.repository;

import com.example.infrastructure.entity.Device;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceRepository extends EntityRepository<Device, Integer> {

}
