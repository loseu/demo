package com.example.infrastructure.util;

import java.time.LocalDate;

public interface DateTimeUtils {

    static LocalDate getCurrentDate() {
        return LocalDate.now();
    }

}
