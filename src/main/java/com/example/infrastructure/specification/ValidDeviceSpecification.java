package com.example.infrastructure.specification;

import com.example.infrastructure.util.DateTimeUtils;
import java.util.Map;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
public class ValidDeviceSpecification implements SearchSpecification {

    private static final String NAME = "device.valid";

    @Override
    public String getName() {
        return NAME;
    }

    @Override
    public Specification getSpecification(Map<String, Object> params) {
        return (root, query, cb) -> {
            var joined = root.join("certificates");

            var now = cb.literal(DateTimeUtils.getCurrentDate());
            var certificateStartDate = joined.get("startDate");
            var certificateExpirationDate = joined.get("expirationDate");

            return cb.between(now, certificateStartDate, certificateExpirationDate);
        };
    }

}
