package com.example.infrastructure.specification;

import java.util.Map;
import org.springframework.data.jpa.domain.Specification;

public interface SearchSpecification {

    String getName();

    Specification getSpecification(Map<String, Object> params);

}
