package com.example.infrastructure.specification;

import java.util.List;
import java.util.Map;
import lombok.RequiredArgsConstructor;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Component;

@Component
@RequiredArgsConstructor
public class SpecificationFactory {

    private final List<SearchSpecification> specifications;

    public Specification create(String name, Map<String, Object> params) {
        return specifications.stream()
                .filter(s -> s.getName().equals(name))
                .findFirst()
                .map(s -> s.getSpecification(params))
                .orElse(null);
    }

}
