package com.example.infrastructure.service.impl;

import com.example.infrastructure.exception.BadRequestException;
import com.example.infrastructure.exception.EntityNotFoundException;
import com.example.infrastructure.entity.BaseEntity;
import com.example.infrastructure.mapper.EntityMapper;
import com.example.infrastructure.repository.EntityRepository;
import com.example.infrastructure.service.CrudService;
import com.example.infrastructure.service.dto.Dto;
import com.example.infrastructure.service.dto.SearchRequest;
import com.example.infrastructure.specification.SpecificationFactory;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.domain.Specification;

@RequiredArgsConstructor
public abstract class AbstractCrudService<T extends BaseEntity, D extends Dto> implements CrudService<D> {

    private final EntityRepository<T, Integer> repository;
    private final EntityMapper<T, D> mapper;
    private final SpecificationFactory specificationFactory;

    @Override
    public D getById(Integer id) {
        var entity = findById(id);
        return mapper.mapToDto(entity);
    }

    @Override
    public Page<D> getAll(SearchRequest request) {
        var pageable = getPageable(request);
        var requestSpecification = request.getSpecification();

        Page<T> entityPage = requestSpecification != null
                ? repository.findAll(getSpecification(requestSpecification), pageable)
                : repository.findAll(pageable);

        return entityPage.map(mapper::mapToDto);
    }

    @Override
    public D create(D request) {
        throw new UnsupportedOperationException();
    }

    @Override
    public D update(D request) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deleteById(Integer id) {
        var entity = findById(id);
        repository.deleteById(entity.getId());
    }

    private Pageable getPageable(SearchRequest request) {
        int size = request.getPageSize();
        int page = request.getPageNumber();

        var sort = request.getSort();
        var direction = sort.getDirection();
        var properties = sort.getProperties().toArray(String[]::new);

        return properties.length > 0
                ? PageRequest.of(page, size, direction, properties)
                : PageRequest.of(page, size);
    }

    private Specification getSpecification(SearchRequest.Specification requestSpecification) {
        String name = requestSpecification.getName();
        var params = requestSpecification.getParams();
        var specification = specificationFactory.create(name, params);
        if (specification == null) {
            throw new BadRequestException("Invalid specification");
        }
        return specification;
    }

    private T findById(Integer id) {
        return repository.findById(id)
                .orElseThrow(EntityNotFoundException::new);
    }

}
