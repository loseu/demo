package com.example.infrastructure.service.impl;

import com.example.infrastructure.entity.Certificate;
import com.example.infrastructure.mapper.EntityMapper;
import com.example.infrastructure.repository.EntityRepository;
import com.example.infrastructure.service.CertificateService;
import com.example.infrastructure.service.dto.CertificateDto;
import com.example.infrastructure.specification.SpecificationFactory;
import org.springframework.stereotype.Service;

@Service
public class CertificateServiceImpl extends AbstractCrudService<Certificate, CertificateDto>
        implements CertificateService {

    public CertificateServiceImpl(
            EntityRepository<Certificate, Integer> repository,
            EntityMapper<Certificate, CertificateDto> mapper,
            SpecificationFactory specificationFactory
    ) {
        super(repository, mapper, specificationFactory);
    }

}
