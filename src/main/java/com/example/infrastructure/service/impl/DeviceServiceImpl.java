package com.example.infrastructure.service.impl;

import com.example.infrastructure.entity.Device;
import com.example.infrastructure.mapper.EntityMapper;
import com.example.infrastructure.repository.EntityRepository;
import com.example.infrastructure.service.DeviceService;
import com.example.infrastructure.service.dto.DeviceDto;
import com.example.infrastructure.specification.SpecificationFactory;
import org.springframework.stereotype.Service;

@Service
public class DeviceServiceImpl extends AbstractCrudService<Device, DeviceDto>
        implements DeviceService {

    public DeviceServiceImpl(
            EntityRepository<Device, Integer> repository,
            EntityMapper<Device, DeviceDto> mapper,
            SpecificationFactory specificationFactory
    ) {
        super(repository, mapper, specificationFactory);
    }

}
