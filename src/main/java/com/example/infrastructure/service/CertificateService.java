package com.example.infrastructure.service;

import com.example.infrastructure.service.dto.CertificateDto;

public interface CertificateService extends CrudService<CertificateDto> {

}
