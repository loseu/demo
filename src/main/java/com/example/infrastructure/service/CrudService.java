package com.example.infrastructure.service;

import com.example.infrastructure.service.dto.Dto;
import com.example.infrastructure.service.dto.SearchRequest;
import org.springframework.data.domain.Page;

public interface CrudService<D extends Dto> {

    D getById(Integer id);

    Page<D> getAll(SearchRequest request);

    D create(D request);

    D update(D request);

    void deleteById(Integer id);

}
