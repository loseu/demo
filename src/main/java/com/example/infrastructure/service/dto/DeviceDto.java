package com.example.infrastructure.service.dto;

import java.util.List;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class DeviceDto implements Dto {

    Integer id;

    List<CertificateDto> certificates;

}
