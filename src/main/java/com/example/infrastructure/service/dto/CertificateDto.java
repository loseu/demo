package com.example.infrastructure.service.dto;

import java.time.LocalDate;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CertificateDto implements Dto {

    Integer id;

    LocalDate startDate;

    LocalDate expirationDate;

}
