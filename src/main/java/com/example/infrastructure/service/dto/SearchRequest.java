package com.example.infrastructure.service.dto;

import java.util.Collections;
import java.util.List;
import java.util.Map;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.springframework.data.domain.Sort;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SearchRequest {

    private static final int DEFAULT_NUMBER = 0;
    private static final int DEFAULT_SIZE = 20;
    private static final SearchSort DEFAULT_SORT = new SearchSort(Collections.emptyList(), Sort.DEFAULT_DIRECTION);

    private int pageNumber = DEFAULT_NUMBER;
    private int pageSize = DEFAULT_SIZE;
    private SearchSort sort = DEFAULT_SORT;
    private Specification specification;

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class SearchSort {
        private List<String> properties;
        private Sort.Direction direction;
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    public static class Specification {
        private String name;
        private Map<String, Object> params;
    }

}
