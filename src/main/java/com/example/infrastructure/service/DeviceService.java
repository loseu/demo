package com.example.infrastructure.service;

import com.example.infrastructure.service.dto.DeviceDto;

public interface DeviceService extends CrudService<DeviceDto> {

}
