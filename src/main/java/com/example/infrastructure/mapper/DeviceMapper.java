package com.example.infrastructure.mapper;

import com.example.infrastructure.entity.Device;
import com.example.infrastructure.service.dto.DeviceDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class DeviceMapper extends AbstractEntityMapper<Device, DeviceDto> {

    public DeviceMapper(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    protected Class<Device> getEntityClass() {
        return Device.class;
    }

    @Override
    protected Class<DeviceDto> getDtoClass() {
        return DeviceDto.class;
    }

}
