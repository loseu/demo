package com.example.infrastructure.mapper;

public interface EntityMapper<T, D> {

    T mapToEntity(D dto);

    D mapToDto(T entity);

}
