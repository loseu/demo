package com.example.infrastructure.mapper;

import com.example.infrastructure.entity.Certificate;
import com.example.infrastructure.service.dto.CertificateDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.stereotype.Component;

@Component
public class CertificateMapper extends AbstractEntityMapper<Certificate, CertificateDto> {

    public CertificateMapper(ObjectMapper objectMapper) {
        super(objectMapper);
    }

    @Override
    protected Class<Certificate> getEntityClass() {
        return Certificate.class;
    }

    @Override
    protected Class<CertificateDto> getDtoClass() {
        return CertificateDto.class;
    }

}
