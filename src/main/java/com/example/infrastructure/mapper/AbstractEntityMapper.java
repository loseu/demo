package com.example.infrastructure.mapper;

import com.example.infrastructure.entity.BaseEntity;
import com.example.infrastructure.service.dto.Dto;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public abstract class AbstractEntityMapper<T extends BaseEntity, D extends Dto> implements EntityMapper<T, D> {

    private final ObjectMapper objectMapper;

    @Override
    public T mapToEntity(D dto) {
        return objectMapper.convertValue(dto, getEntityClass());
    }

    @Override
    public D mapToDto(T entity) {
        return objectMapper.convertValue(entity, getDtoClass());
    }

    protected abstract Class<T> getEntityClass();

    protected abstract Class<D> getDtoClass();

}
