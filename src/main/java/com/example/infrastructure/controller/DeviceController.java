package com.example.infrastructure.controller;

import com.example.infrastructure.service.DeviceService;
import com.example.infrastructure.service.dto.DeviceDto;
import com.example.infrastructure.service.dto.SearchRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/devices")
@RequiredArgsConstructor
public class DeviceController {

    private final DeviceService deviceService;

    @GetMapping("/{id}")
    public DeviceDto getById(@PathVariable("id") Integer id) {
        return deviceService.getById(id);
    }

    @GetMapping
    public Page<DeviceDto> getAll(SearchRequest request) {
        return deviceService.getAll(request);
    }

    @PostMapping
    public DeviceDto create(@RequestBody DeviceDto dto) {
        return deviceService.create(dto);
    }

    @PutMapping("/{id}")
    public DeviceDto update(@PathVariable("id") Integer id, @RequestBody DeviceDto dto) {
        dto.setId(id);
        return deviceService.update(dto);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("id") Integer id) {
        deviceService.deleteById(id);
    }

}
