package com.example.infrastructure.entity;

import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.experimental.SuperBuilder;

@MappedSuperclass
@EqualsAndHashCode
@SuperBuilder
@NoArgsConstructor
@Getter
public abstract class BaseEntity {

    @Id
    @GeneratedValue
    private Integer id;

}
