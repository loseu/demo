package com.example.infrastructure.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

@Entity
@Table(name = "certificate")
@Getter
@SuperBuilder
@NoArgsConstructor
public class Certificate extends BaseEntity {

    @Column(name = "start_date")
    private LocalDate startDate;

    @Column(name = "end_date")
    private LocalDate expirationDate;

    @ManyToOne
    @JoinColumn(name = "device_id")
    @JsonBackReference
    private Device device;


}
